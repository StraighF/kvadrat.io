$(document).ready(function () {
   $('.slider--normal').slick({
       dots: true,
       arrows: false
   });
    $('.slider--arrows').slick({
        dots: true,
        arrows: true
    });


        var amountScrolled = 1000;

        //$('a.back-to-top').fadeIn('slow');

        $(window).scroll(function() {
            console.log($(window).scrollTop());
            if ( $(window).scrollTop() > amountScrolled ) {
                $('a.back-to-bot').fadeIn('medium');
                $('a.back-to-top').fadeIn('medium');
            } else {
                $('a.back-to-top').fadeOut('medium');
                $('a.back-to-bot').fadeOut('medium');
            }
        });

        $('a.back-to-bot').click(function() {
            $('html, body').animate({
                scrollTop: 10000
            }, 1000);
            return false;
        });

        $('a.back-to-top').click(function() {
        $('html, body').animate({
            scrollTop: 0
        }, 1000);
        return false;
    });

});